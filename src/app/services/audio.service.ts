import { Injectable } from '@angular/core';
import {Events} from "@ionic/angular";

@Injectable({
  providedIn: 'root'
})
export class AudioService {
  audio: any;
  constructor(private events: Events) { }

    playAudio(audioFile: string) {
        if (!this.audio) {
          this.audio = new Audio();
        }
        this.audio.src = `./assets/audios/${audioFile}`;
        this.audio.load();
        this.audio.onerror = () => {
            this.events.publish('audio:finish');
        };
        this.audio.onended =  () => {
            this.events.publish('audio:finish');
        };
        this.audio.play();
    }

    stopAudio() {
        this.audio.pause();
    }
    destroyAudio() {
      this.stopAudio();
      delete this.audio;
    }
}
