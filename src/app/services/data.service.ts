import {Injectable} from '@angular/core';

@Injectable({
    providedIn: 'root'
})
export class DataService {
    buildingData = [{
        floorName: 'Piso 1',
        stations: [
            {
                id: 1,
                title: 'Estación 1 - SALA DE INMERSIÓN',
                subtitle: 'Sumergite, Divertite y Aprendé',
                description: `
<p> Convertirse en artistas, conocer reconocidos personajes de la historia son algunas de las experiencias que queremos que disfrutes en este espacio. Vivenciar, experimentar forman parte de nuestras formas de aprender. No dejes de sumergirte y explorar los siguientes proyectos que forman parte de la investigación, la innovación y la transferencia de la Facultad de Informática. En esta estación te invitamos a conocer los siguientes proyectos:
<ol>
<li>
Murales: una aplicación basada en interacción tangible para la creación de murales que combinan el mundo físico y el digital
</li>
<li>
Astrocódigo: un juego 3D para el aprendizaje de conceptos básicos de programación y el desarrollo del pensamiento computacional.
</li>
<li>
Espacio Darwin: en este espacio se muestran tres proyectos que se integran, estos son:
<ul>
<li>
<strong>Ruta Darwin:</strong> el recorrido de Darwin a bordo del Beagle aumentado
</li>
<li>
<strong> HMSBeagleVR:</strong> Una experiencia inmersiva a bordo del HMS Beagle
</li>
<li>
<strong>Ruta Darwin 3D:</strong> conociendo detalles de la vida del científico.
</li>
</ul>
</li>
</ol>
</p>`,
                descriptionAudio: 'audioEstacion1.mp3',
                projects: [
                    {
                        id: 1,
                        title: 'Murales: creación de murales que combinan el mundo físico y digital',
                        description: `
                  <p>
                    <p>
                       Murales presenta una innovación tanto en el modelo de interacción como en su propuesta para la creación de murales digitales. Se trata de una aplicación basada en interacción tangible. Se orienta al ámbito educativo y se propone:
                       </p>
                        <ul>
                            <li>
                                 Desarrollar la creatividad a partir del uso de diferentes materiales, el uso de colores y texturas
                            </li>
                            <li>
                                 Crear sus propios murales experimentando con el mundo digital y físico
                            </li>
                        </ul>
                   <p> Murales ha sido utilizado por diversos públicos en la Expo UNLP 2019, las Jornadas de Ciencia y Tecnología de la Facultad de Informática 2019 y en el concurso INNOVAR 2019.
                    </br>
                    Posibilita jugar con objetos físicos (por ejemplo, provenientes de la naturaleza) para la creación de murales y para la configuración cuenta de colores de fondo, transparencias, tamaños de los objetos, cuenta con herramientas físicas de configuración creadas utilizando una impresora 3D.
                    </p>
                  </p>
`,
                        descriptionAudio: 'audioMurales.mp3',
                        authors: `
<p>
Alumnos: Franco Pirondo, Bárbara Ibañez, Bárbara Corro, Mauricio Nordio
</br>
Docentes: Verónica Artola, Cecilia Sanz
</br>
Se integra a un proyecto del III LIDI vinculado a la investigación de paradigmas de interacción persona ordenador, aplicados a espacios educativos. 
</p>`,
                        mainImage: null,
                        mainVideo: 'videoMurales.mp4',
                        images: [
                            {fileName: 'murales-imagen1.png'},
                            {fileName: 'murales-imagen2.png'},
                            {fileName: 'murales-imagen3.png'},
                            {fileName: 'murales-imagen4.png'},
                            {fileName: 'murales-imagen5.png'},
                            {fileName: 'murales-imagen6.png'},
                        ]
                    },
                    {
                        id: 2,
                        title: 'AstroCódigo: un videojuego 3D para el desarrollo del pensamiento computacional',
                        description: `<p> Astrocódigo es un videojuego 3D para enseñar y aprender conceptos básicos de programación. Está orientado a estudiantes de nivel medio, docentes, y público en general, interesado en desarrollar habilidades relacionadas al pensamiento computacional. Ha sido utilizado en el curso de Ingreso a las carreras de Informática de la Facultad de Informática de la UNLP, en el desarrollo de competencias computacionales en docentes en la Universidad de Costa Rica, en la Expo UNLP con estudiantes de nivel medio y en talleres con alumnos de nivel medio en el marco del Programa Nexos.</p>
</br>
<p>Disponible en: <strong>www.astrocodigo.com</strong></p>`,
                        descriptionAudio: 'audioAstrocodigo.mp3',
                        authors: `<p>
Desarrollado por Pablo Miceli y Javier Brione en el marco de su tesina de licenciatura y dirigido por los profesores Cecilia Sanz y Verónica Artola.
</br>Se vincula con el proyecto de investigación del III LIDI relacionado con Tecnologías digitales en Escenarios educativos.
</p>`,
                        mainImage: null,
                        mainVideo: 'videoAstrocodigo.mp4',
                        images: [
                            {fileName: 'astrocodigo-imagen1.png'},
                            {fileName: 'astrocodigo-imagen2.png'},
                            {fileName: 'astrocodigo-imagen3.png'},
                            {fileName: 'astrocodigo-imagen4.png'},
                            {fileName: 'astrocodigo-imagen5.png'},
                        ]
                    },
                    {
                        id: 3,
                        title: 'Espacio Darwin: un conjunto de aplicaciones con Realidad Aumentada y Virtual para conocer al científico y naturalista Charles Darwin ',
                        description: `
<p> En este espacio se pueden encontrar tres proyectos integrados para aportar a una muestra itinerante sobre el científico y naturalista Charles Darwin, organizada por la Subsecretaría de Gestión y Difusión del Conocimiento, Ministerio de Ciencia, Tecnología e Innovación de la provincia de Buenos Aires. La exposición aborda la figura de Charles Darwin desde una perspectiva histórica, cultural y científica. Los proyectos desarrollados se gestan en el marco de la investigación, innovación y transferencia del III LIDI, en cooperación con la Subsecretaría antes mencionada.</p>
<p>Los tres proyectos integrados son:</p>
<br><p><strong> HMSBeagleVR: Una experiencia inmersiva a bordo del HMS Beagle </strong><br>La aplicación brinda una experiencia inmersiva a lo largo de la embarcación y el camarote utilizado por Darwin, permitiendo su recorrido mediante gafas de realidad virtual e incluso pudiendo interactuar con ciertos objetos de la escena, a fin de obtener información detallada sobre estos elementos.
<br>La aplicación aporta una nueva forma de interacción para la muestra itinerante, dando un nuevo ritmo al recorrido de la visita en la que el conocimiento se convierte en vivencia personal y a la vez socialmente compartida.</p>
<br><p><strong> Ruta Darwin: </strong><br>
Ruta Darwin es un juego con realidad aumentada para dispositivos móviles. Se trata de <u> un juego tipo</u> exploratorio cuyo objetivo <u> es completar un álbum de medallas </u> a partir del recorrido por diferentes puertos en los que estuvo Darwin en su viaje a bordo del Beagle. En cada puerto es posible acceder a un video, contestar una trivia y ganar una medalla para completar el álbum. Desde el punto de vista educativo, se espera lograr, entre otros objetivos, que los alumnos conozcan a Darwin de una manera más vivencial, identifiquen especies encontradas por el científico, y reconozcan instrumentos utilizados por científicos de su época.
<br> Tanto Ruta Darwin como HMSBeagleVR han formado parte de la muestra itinerante en las ciudades de Bahía Blanca, Tandil y Mar del Plata con más de 30000 personas que la han visitado. </p>
<br><p><strong>Ruta Darwin 3D </strong><br>Es un <i>spin off</i> (o desprendimiento) del proyecto original Ruta Darwin, donde se propone el desafío de trabajar con objetos 3D en la detección, en el marco de un proyecto conjunto con alumnos de la facultad.</p>`,
                        descriptionAudio: 'audioEspacioDarwin.mp3',
                        authors: `<p><strong>Ruta Darwin:</strong> aplicación de realidad aumentada realizada por Agustín Lizarralde en el marco de su trabajo final de Licenciatura. Coordinada por Cecilia Sanz y Gladys Gorga del III LIDI. El guionado se realizó conjuntamente con Rebeca Kraselsky (Subsecretaría de Gestión y Difusión del Conocimiento, Ministerio de Ciencia, Tecnología e Innovación de la provincia de Buenos Aires). En el diseño participaron Abril Buffarini del III LIDI, Elina Beltrán de la Subsecretaría antes mencionada, y la ilustración de Darwin estuvo a cargo de Ayelén García (Facultad de Bellas Artes). En la locución de los videos participó el programa ADN Ciencia de Radio UNLP, voz a cargo del Locutor Nacional  Alfredo Tangorra.
</br><strong>Recorrido 3D del Beagle:</strong> desarrollada por Federico Cristina, Sebastián Dapoto, y Pablo Thomas, investigadores del III LIDI.
</br><strong>Ruta Darwin 3D:</strong> desarrollado por los alumnos de Ingenieria en Computación Leonardo Loza Bonora y Tomás Naiouf en el marco de la convocatoria 2019 de Proyecto con Alumnos. Modelo de Darwin 3D a cargo de Pedro Pousada. Coordinación del proyecto a cargo de investigadores del III LIDI: Natalí Salazar Mesía, Federico Archuby y Cecilia Sanz
</p>`,
                        mainImage: null,
                        mainVideo: 'videoRD.mp4',
                        images: [
                            {fileName: 'darwin-imagen1.png'},
                            {fileName: 'darwin-imagen2.png'},
                            {fileName: 'darwin-imagen3.png'},
                            {fileName: 'darwin-imagen4.png'},
                            {fileName: 'darwin-imagen5.png'},
                            {fileName: 'HMSBeagleVR-01.png'},
                            {fileName: 'HMSBeagleVR-02.png'},
                            {fileName: 'HMSBeagleVR-03.png'},
                        ]
                    },
                ]
            },
            {
                id: 2,
                title: 'Estación 2',
                subtitle: 'Jugar, Transferir, Aportar',
                description: null,
                descriptionAudio: 'audioEstacion2.mp3',
                projects: [
                    {
                        id: 1,
                        title: 'Automatizacion en Usabilidad Web',
                        description: `

<p>Nos hemos acostumbrado a utilizar la web en muchas de nuestras actividades cotidianas. A pesar de los grandes avances y mejoras que ha experimentado, con frecuencia encontramos dificultades para usarla debido al mal diseño de la interacción (hoy conocido como usabilidad o diseño de la experiencia de usuario). Este proyecto busca automatizar las actividades de medición y mejora de la usabilidad en aplicaciones web. Contamos actualmente con dos herramientas innovadoras desarrolladas como prueba de concepto: Kobold  que se utiliza para detectar automáticamente problemas de usabilidad, conocidos también como “usability smells”, e incluso para resolverlos mediante la técnica de “usability refactoring”. Y Tycho, herramienta que sirve para diseñar y correr pruebas de usuario remotamente, permitiendo crear tareas coordinadas entre múltiples usuarios, y capturar tiempos y otras medidas automáticamente. Actualmente estamos trabajando en medir el esfuerzo de los usuarios al utilizar formularios de forma completamente automatizada, mediante técnicas de Machine Learning. Ambas herramientas se encuentran disponibles bajo licencia open source.</p> 
<br>
<p>Sitio web de Kobold: https://autorefactoring.lifia.info.unlp.edu.ar   
Kobold open source: https://github.com/juliangrigera/Kobold
Kobold en Youtube: http://bit.ly/kb-video1, http://bit.ly/kb-video2 
</p>
<br>
<p>
Sitio web de Tycho: http://tycho.lifia.info.unlp.edu.ar
Tycho open source: https://github.com/juliangrigera/Tycho

</p>`,
                        descriptionAudio: 'audioAutomatizacion.mp3',
                        authors: `<p>Contacto: julian.grigera@lifia.info.unlp.edu.ar </p>`,
                        mainImage: 'automatizacion-imagen1.png',
                        mainVideo: null,
                        images: null
                    },
                    {
                        id: 2,
                        title: 'BPAi-una plataforma colaborativa sobre buenas prácticas agrícolas intensivas',
                        description: `<p>
BPAi es una plataforma colaborativa de conocimiento sobre buenas prácticas agrícolas intensivas. Son prácticas más amigables con el ambiente y menos nocivas para los recursos naturales, la salud de los trabajadores y la inocuidad de los productos destinados a la alimentación. Este proyecto se ejecuta en conjunto con la Facultad de Ciencias Agrarias y Forestales de la UNLP (FCAyF). En el se desarrollan líneas de investigación en tecnologías de soporte a la colaboración, manejo de conocimiento, ciencia ciudadana, ludificación tangible y bio arte. Adicionalmente, este proyecto se deriva de los proyectos Agroknowledge (del programa AgregandoValor de la Secretaría de Políticas Universitarias) y RUC-APS del programa Horizonte 2020 de la Unión Europea. </p>`,
                        descriptionAudio: 'audioBpai.mp3',
                        authors: `<p>Contacto: Diego Torres diego.torres@lifia.info.unlp.edu.ar</p>
<p>Participantes: Diego Torres, Julieta Lombardelli, Blas Buttera, Mariana del Pino (FCAyF), Susana Gamboa (FCAyF)</p>`,
                        mainImage: 'BPAi-imagen1.png',
                        mainVideo: null,
                        images: null
                    },
                    {
                        id: 3,
                        title: 'Juegos Móviles basados en Posicionamiento: el desafío de los espacios indoors',
                        description: `<p>Este proyecto, que se enmarca en el programa de proyectos de innovación con alumnos de la Facultad de Informática, tiene como objetivo explorar los desafíos involucrados tanto a nivel de diseño como de implementación de Juegos Móviles basados en Posicionamiento, en particular, para espacios indoor. Se pone en práctica una experiencia de co-diseño in-situ en un espacio indoor, la cual permite aprender cómo conducir este tipo de co-diseño, como así también como combinar métodos de Design Thinking con una herramienta de autor. A partir de lo aprendido, se extiende una herramienta de autor para poder co-diseñar in-situ en un espacio indoor Juegos Móviles basados en Posicionamiento. Usando la extensión creada en el marco de este proyecto, se creó un juego móvil que fue presentado en la V Expo Ciencia y jugado por los participantes de la misma.</p>`,
                        descriptionAudio: 'audioJuegosMoviles.mp3',
                        authors: `<p>Dra. Cecilia Challiol ceciliac@lifia.info.unlp.edu.ar
Alumnos participantes: Franco M. Borrelli, Francisco Goin Plexevi, Candela M. Rouaux Servat, Diego H. Orellano
 <br>
Colaborador externo: Facundo I. Mendiburu
</p>`,
                        mainImage: 'juegosMoviles-imagen1.png',
                        mainVideo: null,
                        images: null
                    },
                    {
                        id: 4,
                        title: 'Mobisaar',
                        description: `<p>Mobisaar es un proyecto de transferencia de tecnología del LIFIA, cuyo objetivo es desarrollo de aplicaciones móviles basado en el concepto de Smart Cities a ser desplegado en Alemania. El objetivo es asistir a personas mayores o con alguna dificultad a movilizarse dentro de ciudades utilizando rutas inteligentes y guías que los acompañan en cada uno de los trayectos utilizando transporte público.,</p>`,
                        descriptionAudio: 'audioMobisaar.mp3',
                        authors: `Contacto: Javier Bazzocco javier.bazzocco@lifia.info.unlp.edu.ar`,
                        mainImage: 'mobisaar-imagen1.png',
                        mainVideo: null,
                        images: null
                    },
                    {
                        id: 5,
                        title: 'RUC-APS : Mejora e implementación de soluciones  TIC basadas en el conocimiento, en condiciones de alto riesgo e incertidumbre para los sistemas de producción agrícola',
                        description: `<p>
Hoy en día, la agricultura recibe cada vez más presión para satisfacer la creciente necesidad de alimentos en todo el mundo. Se hace necesario aumentar su resistencia y capacidad de recuperación frente a los cambios bruscos en términos de calidad de los recursos, su cantidad y disponibilidad, especialmente durante condiciones climáticas turbulentas o comportamiento inesperado del entorno. Se requieren soluciones integradas que sean capaces de hacer frente a los retos de colaboración entre los diferentes agentes involucrados, desde de las primeras fases de la agricultura como el diseño de productos, a la cosecha, hasta las fases posteriores a la cosecha y procesamiento final. RUCAPS es un proyecto multidisciplinar, financiado por la unión Europea, con participantes de la industria y la ciencia de Argentina, Chile, España, Inglaterra, Francia, Italia y Polonia. Se propone realizar investigación de alto impacto, integrando aspectos de requerimientos realistas de producción, alternativas de gestión de tierra para varias escalas, condiciones impredecibles de clima y ambiente, e innovación en sistemas de producción agrícola y su impacto en los usuarios. 

Sitio web de RUC-APS: https://www.ruc-aps.eu  
</p>`,
                        descriptionAudio: 'audioRucAps.mp3',
                        authors: `<p>Contacto: alejandro.fernandez@lifia.info.unlp.edu.ar </p>`,
                        mainImage: 'ruc-imagen1.png',
                        mainVideo: null,
                        images: null

                    },
                ]
            },
            {
                id: 3,
                title: 'Estación 3',
                subtitle: 'Analizá, Conocé e Innová',
                description: `
                <p>La sociedad se ve atravesada por las tecnologías digitales y se han ido entramando en todos sus procesos. Escenarios impensados hoy han empezado a ser parte de la sociedad:<ul><li>Gobernanza digital</li><li>Ciudades digitales</li><li>Cuidado del medio ambiente y prevención de catástrofes e inundaciones</li>
</ul>Por otra parte, las emociones propias de los seres humanos podrían mostrar cómo las personas perciben, y participan con tecnologías digitales. El campo de la Computación afectiva es un área de investigación e innovación actual. Conocé y explorá los proyectos de este espacio.
En la estación 1 de este itinerario podrán ver:<ol><li>Máquina de voto electrónico y las estrategias diseñadas para su integración en procesos de votación: un proyecto ya en marcha hace uno años y que forma parte de las líneas de investigación en gobernanza digital.</li><li>Tablero de control energético SINDI: se presenta el diseño de un <strong>Sistema Inteligente Distribuido</strong> capaz de obtener la información de los diferentes sectores de un edificio y, de esta forma, tomar medidas que generen un ahorro energético.  </li><li>Actuados: un juego relacionado con la detección de las emociones de las personas que se relaciona con la investigación en sistemas inteligentes y computación afectiva.</li></ol></p>`,
                descriptionAudio: 'audioEstacion3.mp3',
                projects: [
                    {
                        id: 1,
                        title: 'Máquina de Voto Electrónico',
                        description: `
                        <strong>MÁQUINA DE VOTO ELECTRÓNICO</strong>
<p><strong>La innovación se vincula con las tecnologías organizativas.</strong>  Se han desarrollado máquinas de voto electrónico y los procedimientos necesarios para que sean utilizadas en contextos reales. En el marco del proyecto <strong>E-Voto</strong> perteneciente al III LIDI, desde el año 2007, se ha utilizado los equipos en las elecciones de los distintos claustros de la Facultad de informática y en varios eventos de ciencia y tecnología organizados por la CIC, la Facultad de Informática y el Gobierno de la Provincia de Buenos Aires. Además el proyecto incluye servicios de diferentes tipos de elecciones a través de internet.</p>
<p><strong>El equipo de voto electrónico que se encuentra</strong> en la planta baja se vincula con los proyectos de la estación 1, en donde se presentan aplicaciones para la sociedad. En esta ocasión la utilizaremos para votar la innovación que más te gustó al finalizar el recorrido.</p>
`,
                        descriptionAudio: 'audioVoto.mp3',
                        authors: `
<strong>Integrantes del proyecto</strong>
 <ul style="list-style-type:none">
         <li>Dirección: Patricia Pesado</li>
         <li>Franco Chichizola, Ariel Pasini,</li>
         <li>Cesar Estrebou, Nicolas Galdamez,</li>
         <li>Juan Manuel Paniego, Martin Pi Puig, Rocio Muñoz,</li>
         <li>Ismael Rodriguez,  Sebastian Rodriguez Eguren, Adrian Pousa</li>
      </ul>
`,
                        mainImage: 'voto-imagen3.png',
                        mainVideo: null,
                        images: [
                            {fileName: 'voto-imagen1.png'},
                            {fileName: 'voto-imagen2.png'},
                            {fileName: 'voto-imagen3.png'},
                            {fileName: 'voto-imagen4.png'},
                        ]
                    },
                    {
                        id: 2,
                        title: 'Tablero de Control Energético SINDI',
                        description: `
                        <p><strong>SINDI: Sistema Inteligente Distribuido para el Control y Optimización del Consumo Eléctrico</strong> <br>Actualmente, la sociedad tiene una alta demanda energética para desarrollar sus tareas cotidianas, generando un elevado gasto económico y produciendo un alto impacto ambiental, debido a que el mayor porcentaje de la energía primaria en Argentina, proviene de hidrocarburos. Las instituciones públicas, constituyen un escenario significativo para conseguir importantes reducciones en el consumo de energía y emisiones contaminantes. Una forma de optimizar el consumo, es la instalación de un <strong>Sistema Inteligente Distribuido</strong> capaz de obtener la información de los diferentes sectores del edificio y, de esta forma, tomar medidas que generen un ahorro energético.  El sistema de Control y Optimización del Consumo Eléctrico diseñado y desarrollado en este proyecto se divide en tres niveles: <ol><li>En el primer nivel, se ubican las Unidades Locales de Procesamiento Inteligente (ULPIs) que controlan los diferentes ambientes, por medio de un conjunto o red de sensores capaces de captar eventos del ambiente y, un conjunto de actuadores, para el control de los artefactos eléctricos.</li><li>En el segundo nivel, se encuentran los Servidores Locales (SL). Por cada edificio que se quiera monitorear, se ubica un SL el cual recolecta los datos de todas las ULPIs del espacio que controla, obteniendo estadísticas del consumo de cada ambiente.</li><li>En el tercer nivel, se encuentra un Servidor Cloud (SC), el cual recolecta los datos provenientes de los diferentes SLs, los cuales podrían estar geográficamente distribuidos.</li></ol>Un posible ámbito de aplicación es la Universidad Nacional de La Plata (UNLP) que está compuesta por 17 facultades, más de 100 edificios y más de 1200 espacios físicos, cubriendo más de 400.000 mts2 edificados y distribuidos en diferentes sectores de la ciudad de La Plata.</p>`,
                        descriptionAudio: 'audioTablero.mp3',
                        authors: `<p>Se trata de un proyecto desarrollado en el marco del III LIDI. El equipo de investigadores que lleva a cabo el proyecto está conformado por Laura De Giusti; Franco Chichizola; César Estrebou. Juan Manuel Paniego; Martín Pi Puig; Leandro Libutti; Sebastián Eguren Rodríguez; Julieta Lanciotti; Joaquín De Antueno y Santiago Medina.</p>`,
                        mainImage: null,
                        mainVideo: 'videoTablero.mp4',
                        images: [
                            {fileName: 'tablero-imagen1.png'},
                            {fileName: 'tablero-imagen2.png'},
                            {fileName: 'tablero-imagen3.png'},
                            {fileName: 'tablero-imagen4.png'},
                        ]
                    },
                    {
                        id: 3,
                        title: 'Juego Actuados - Reconocimiento automático de emociones en video',
                        description: `
                        <p>
                        Actuados es un juego cooperativo y educativo para que puedas identificar emociones. La dinámica propone que un grupo de jugadores gane puntos actuando distintas emociones.
</p><br>
<p>El juego cuenta con un sistema de Inteligencia Artificial que puede reconocer las emociones faciales de los jugadores y asignarles un puntaje. El sistema está entrenado con actores famosos de todo el mundo. Acercate al sector del proyecto y empezá a jugar.
</p>`,
                        descriptionAudio: 'audioActuados.mp3',
                        authors: `<p>Este proyecto fue realizado en el marco del III LIDI, con la participación de alumnos y docentes investigadores. Los autores del trabajo son:
<ul>
<li>
Ulises Jeremías Cornejo Fandos
</li>
<li>
Gastón Ríos
</li>
<li>
Facundo Quiroga
</li>
<li>
Franco Ronchetti
</li>
</ul>
</p>`,
                        mainImage: null,
                        mainVideo: 'videoActuados.mp4',
                        images: [
                            {fileName: 'actuados-imagen1.png'},
                            {fileName: 'actuados-imagen2.png'},
                            {fileName: 'actuados-imagen3.png'}
                        ]
                    }
                    ]
            },
            {
                id: 4,
                title: 'Estación 4',
                subtitle: 'Participá, Conocé y Explorá',
                description: `
                <p>La disciplina Informática evoluciona rápidamente. La forma de interacción entre las personas y las computadoras ha ido ofreciendo nuevas oportunidades. Así la Realidad Aumentada (RA) posibilita enriquecer el entorno real a partir de información digital. <br>La Realidad Virtual (RV), en cambio, sumerge a la persona en un nuevo escenario, sintético, en el que las simulaciones pueden ayudar a experimentar lo imposible. Aún se está trabajando en acercar estas posibilidades de interacción en diferentes actividades de la sociedad. Los dispositivos móviles han incrementado su acercamiento y ofrecen también nuevas formas de interacción para el acceso a los sistemas informáticos.<br>En esta esta estación podrás ver los siguientes proyectos:<ol><li><strong>INFOUNLP3D:</strong> una aplicación que posibilita un recorrido 3D de la Facultad de Informática. Puede utilizarse con gafas de Realidad Virtual o en una PC a través de un navegador web. Ha sido creada para acercar a los alumnos interesados al espacio de la Facultad</li><li><strong>HuVi:</strong> una aplicación móvil que posibilita experimentar la Realidad Virtual para acercar patrimonios de la Argentina, a través del juego y la experiencia. Se ha utilizado principalmente con niño/as y al momento, se trabaja con un recorrido del Parque Nacional Iguazú.</li><li><strong>EPRA:</strong> es un material educativo hipermedial con actividades de RA para aprender sobre estructuras de control y programación.</li></ol></p>`,
                descriptionAudio: 'audioEstacion4.mp3',
                projects: [
                    {
                        id: 1,
                        title: 'INFOUNLP3D: Recorrido 3D de la Facultad de Informática',
                        description: `<p>
InfoUNLP3D es un prototipo móvil que mediante un ambiente tridimensional sirve de guía para los estudiantes en sus primeras experiencias dentro de la Facultad de Informática de la UNLP. 
<br>La herramienta sitúa a los estudiantes dentro de un modelo 3D virtual de la facultad, presentándoles una serie de posibles recorridos hacia los puntos de interés más relevantes dentro del edificio.
<br>El prototipo permite además recorrer los espacios virtuales de forma libre, para poder conocer todas las áreas de la facultad, aún antes de visitar físicamente el edificio.
<br>La aplicación a su vez permite conocer las clases que se estan dictando en las diferentes aulas. Solo es necesario acercarse a un aula y se visualiza la información correspondiente a la clase actual. A su vez, es posible navegar por los diferentes días y horarios para conocer todas las clases que se dictan en una determinada aula.

</p>`,
                        descriptionAudio: 'audioInfoUNLP3D.mp3',
                        authors: `<p>
El proyecto ha sido desarrollado en el marco del III LIDI. Han participado en el desarrollo: Sebastián Dapoto, Federico Cristina y Pablo Thomas en la coordinación.</p>`,
                        mainImage: null,
                        mainVideo: 'videoINFO3D.mp4',
                        images: [
                            {fileName: 'InfoUNLP3D-01.png'},
                            {fileName: 'InfoUNLP3D-02.png'},
                            {fileName: 'InfoUNLP3D-03.png'},
                            {fileName: 'InfoUNLP3D-04.png'},
                            {fileName: 'InfoUNLP3D-05.png'}
                        ]
                    },
                    {
                        id: 2,
                        title: 'HuVI: Huellas Virtuales presenta una aplicación de realidad virtual para reconocer patrimonios de la Argentina',
                        description: `
<p>Huvi se presenta como una INNOVACIÓN que vincula diversos ámbitos disciplinares. Surge a partir de un <strong> proyecto de extensión </strong> en conjunto con la <strong> Facultad de Ciencias Económicas y Bellas Artes </strong>. Combina una experiencia lúdico - educativa, con el recorrido de un patrimonio como el Parque Nacional Iguazú, y el conocimiento a través de la experiencia. La aplicación fue creada en el marco de una tesis de la Maestría en Tecnología Informática aplicada en Educación, y es una aplicación móvil, en la que se interactúa con gafas de RV.</p>
<p>Para interactuar en la aplicación se usa la mira y se enfoca objeto con el que se quiere interactuar. </p>
<p>La aplicación ha sido utilizada durante 2018 y 2019 en talleres en el marco del proyecto de extensión con niño/as de diversas escuelas de la región de La Plata.</p>
<p>¿Querés conocerla? <strong>Ponete las gafas y explorá.</strong></p>`,
                        descriptionAudio: 'audioHuvi.mp3',
                        authors: `<p>Esta aplicación ha sido desarrollada en el marco de un proyecto de Extensión de la Facultad de Ciencias Económicas, Informática y Bellas Artes.
<br>
El desarrollo estuvo a cargo de: Yesica Chirinos (como parte de su tesis de Maestría en Tecnología Informática Aplicada en Educación), coordinado por Cecilia Sanz (Informática), y Sebastián Dapoto (Informática). Los contenidos de videos e imágenes y parte del guionado estuvo a cargo de Gabriel Comparato (Cs. Económicas), Ana Rucci (Cs. Económicas), y Leonardo Gabriel Gonzalez (Bellas Artes. La creación de los avatares y otros aspectos del diseño visual de la aplicación estuvieron a cargo de Ayelén García (Bellas Artes).
</p>`,
                        mainImage: null,
                        mainVideo: 'videoHuvi.mp4',
                        images: [
                            {fileName: 'huvi-imagen1.png'},
                            {fileName: 'huvi-imagen2.png'},
                            {fileName: 'huvi-imagen3.png'},
                            {fileName: 'huvi-imagen4.png'},
                            {fileName: 'huvi-imagen5.png'},
                        ]
                    },
                    {
                        id: 3,
                        title: 'EPRA: actividades educativas con Realidad Aumentada para aprender conceptos básicos de programación',
                        description: `<p>Es un material educativo hipermedial que integra actividades con Realidad Aumentada para el aprendizaje de las estructuras de control. Se ha desarrollado como parte de un proyecto del III LIDI y se ha utilizado en el curso de ingreso, en cátedras de primer año y en actividades de articulación entre escuela y universidad. ¡Acercate y probalo!</p>`,
                        descriptionAudio: 'audioEpra.mp3',
                        authors: `<p>Este proyecto ha sido desarrollado como parte de una beca UNLP TIPO A. Los autores son Lic. Natalí Salazar Mesía, Cecilia Sanz y Gladys Gorga.
</p><p>Se inserta en un proyecto de investigación del III LIDI, vinculado a temas de tecnologías digitales en escenarios educativos.
</p>`,
                        mainImage: null,
                        mainVideo: 'videoEPRA.mp4',
                        images: [
                            {fileName: 'EPRA-imagen1.png'},
                            {fileName: 'EPRA-imagen2.png'},
                            {fileName: 'EPRA-imagen3.png'},
                            {fileName: 'EPRA-imagen4.png'},
                        ]
                    },
                   ]
            },
            {
                id: 5,
                title: 'Estación 5',
                subtitle: 'Conocé, Analizá y Explorá',
                description: `<p> La evolución en la formas de interacción entre las personas y las computadoras propone el uso de interfaces más naturales. Aprovechar las posibilidades y habilidades que las personas  ya han desarrollado en su entorno físico es una meta. La interacción tangible busca involucrar las manipulaciones físicas de objetos del entorno para controlar información digital. Los entornos 3D posibilitan recuperar el sentido de la espacialidad. Acercar estas posibilidades a los docentes constituye una meta. Por otra parte, los juegos educativos combinan el entretenimiento con objetivos de aprendizaje, su diseño involucra una mirada interdisciplinaria donde la jugabilidad, la diversión y el aprendizaje deben entramarse.  Para ello te presentamos a:
<ul><li><strong>FraccionAR:</strong> Se trata de un juego para el aprendizaje de fracciones, considerando diferentes representaciones de éstas. Se utiliza una mesa interactiva y objetos físicos (juguetes) para la interacción.</li>
<li><strong>EDIT:</strong> una herramienta que posibilita la creación de actividades educativas sobre la mesa interactiva por parte de docentes. EDIT se propone acercar las tecnologías de interacción tangible a personas no expertas en Informática.</li>
<li><strong>Desafiate:</strong> un juego para dispositivos móviles de preguntas y respuestas para la autoevaluación de los estudiantes. Se integra con el entorno virtual de enseñanza y aprendizaje IDEAS donde los docentes crean autoevaluaciones que son tomadas para el juego.</li></ul> </p>`,
                descriptionAudio: 'audioEstacion5.mp3',
                projects: [
                    {
                        id: 1,
                        title: 'FraccionAR: un juego basado en interacción tangible para aprender sobre fracciones',
                        description: `<p> Se trata de un juego para el aprendizaje de fracciones, considerando diferentes representaciones de éstas. La dinámica del juego utiliza objetos físicos sobre una mesa interactiva, donde los objetos representan las fracciones. Por ejemplo, porciones de una pizza o de un chocolate. El juego propone una competencia entre dos participantes o dos equipos donde deben asociar fracciones con el/los objeto/s físico/s que las representan, pudiendo existir diferentes asociaciones posibles para la misma fracción. La dinámica presenta una serie de desafíos en los que cada jugador compite simultáneamente en un área de la mesa. Cada desafío tiene un límite de tiempo para contestar a la consigna de asociación.  El juego ha sido presentado en el marco de la Expo UNLP durante 2017, donde fue utilizado por diferentes jóvenes de nivel medio. Además, fue presentado en la Expo Ciencia y Tecnología de la Facultad de Informática de la UNLP, y se trabajó en talleres con niños de primaria. </p>`,
                        descriptionAudio: 'audioFraccionAR.mp3',
                        authors: `<p>Mauricio Nordio, Verónica Artola y Cecilia Sanz </br>
Formó parte de un proyecto con docentes y alumnos </br>
Se desarrolla como parte de las líneas de I+D+I del III LIDI </p>`,
                        mainImage: null,
                        mainVideo: 'videoFraccionAR.mp4',
                        images: [
                            {fileName: 'fraccionar-imagen1.png'},
                            {fileName: 'fraccionar-imagen2.png'},
                            {fileName: 'fraccionar-imagen3.png'},
                            {fileName: 'fraccionar-imagen4.png'},
                            {fileName: 'fraccionar-imagen5.png'},
                        ]
                    },
                    {
                        id: 2,
                        title: 'EDIT: editor de actividades educativas basadas en  Interacción Tangible y una mesa interactiva',
                        description: `<p> El editor EDIT también forma parte de este  proyecto MIDEA (Mesa interactiva para estudiantes en Acción). Es una aplicación web que corre sobre una PC, y a través de plantillas, posibilita la creación de presentaciones (contenido en texto, texto e imagen, video) y actividades del tipo de asociación simple para trabajar con la mesa interactiva y el uso de objetos físicos. Es decir se pueden definir área de interacción sobre la mesa y asociarles qué objeto se correspondería con cada área y una retroalimentación.</p>
                            </br><p> EDIT ha sido utilizada en el marco de un taller sobre el tema Interacción Tangible con docentes en el CIyTT, en 2019. También fue seleccionado el proyecto MIDEA para el concurso INNOVAR 2019 y allí se presentó EDIT durante la exposición. </p>`,
                        descriptionAudio: 'audioEdit.mp3',
                        authors: `<p> Verónica Artola y Cecilia Sanz </br>
Forma parte de los proyectos del III LIDI y se realizó en el marco de la tesis doctoral de Verónica Artola (Becaria Doctoral CONICET) </p>`,
                        mainImage: null,
                        mainVideo: 'videoEdit.mp4',
                        images: [
                            {fileName: 'edit-imagen1.png'},
                            {fileName: 'edit-imagen2.png'},
                            {fileName: 'edit-imagen3.png'},
                            {fileName: 'edit-imagen4.png'},
                            {fileName: 'edit-imagen5.png'},
                        ]
                    },
                    {
                        id: 3,
                        title: 'Desafiate: reconocé cuánto sabés a través de un juego de preguntas y respuestas para móviles',
                        description: `<p> Desafiate es un juego serio de preguntas y respuestas para dispositivos móviles orientado a la autoevaluación de los estudiantes. El juego ha sido desarrollado para un uso general, en vez de enfocarse en un nivel educativo o disciplina en particular, con lo cual puede ser aprovechado por docentes y estudiantes de todo ámbito. Desafiate fue desarrollado como tesina de grado en la Facultad de Informática de la Universidad Nacional de La Plata.</p>
                           </br><p>El principal objetivo de Desafiate es el de brindar a los estudiantes una forma de autoevaluación menos estresante y más motivante que otros métodos tradicionales, y brindarle a los docentes una herramienta que sea aplicable más allá de la disciplina o el nivel educativo. </p>`,
                        descriptionAudio: 'audioDesafiate.mp3',
                        authors: `<p> Federico Archuby y Cecilia Sanz </br>
Creado como parte un proyecto del III LIDI y en el marco de una beca TIPA A de UNLP.</p>`,
                        mainImage: null,
                        mainVideo: 'videoDesafiate.mp4',
                        images: [
                            {fileName: 'desafiate-Imagen1-a.png'},
                            {fileName: 'desafiate-Imagen1.png'},
                            {fileName: 'desafiate-Imagen2.png'},
                            {fileName: 'desafiate-Imagen3.png'},
                        ]
                    },
                ]
            },
            {
                id: 6,
                title: 'Estación 6',
                subtitle: 'Compartir, Experimentar y Conocer',
                description: null,
                descriptionAudio: 'audioEstacion6.mp3',
                projects: [
                    {
                        id: 1,
                        title: 'Por una Web Inclusiva',
                        description: `<p>El proyecto “Por una Web Inclusiva” tiene por objetivo concientizar a jóvenes de escuelas secundarias, técnicas y medias, sobre la accesibilidad web. En este sentido, promueve la formación de personas conscientes  sobre la diversidad e inculcar el compromiso por el desarrollo de sitios que puedan ser utilizados por todas las personas, más allá de las limitaciones físicas, mentales, intelectuales o sensoriales que posean.</p>
<p> De esta manera, se hace eco de la Ley nacional 26653 sobre los contenidos de las páginas web aprobada en  2010 y sobre la Ley provincial 15115 de accesibilidad de la Provincia de Buenos Aires, aprobada en diciembre de 2018.</p>
 <strong>En este vínculo, los estudiantes se convierten en:</strong>
<ul>
<li>
Observadores de las barreras que existen para las personas con discapacidad que aun están presentes en las tecnologías informáticas y de comunicación.  
</li>
<li>
Promotores de las leyes sobre accesibilidad
</li>
<li>
Testers de sitios para comprobar su nivel de accesibilidad.  
</li>
<li>
Conocedores de cómo  aplicar los estándares internacionales para lograr el desarrollo  web accesible.
</li>
</ul>  
 `,
                        descriptionAudio: 'audioWebInclusiva.mp3',
                        authors: `<p>Laboratorio de Investigación en Nuevas Tecnologías Informáticas (LINTI)</p>`,
                        mainImage: null,
                        mainVideo: 'videoWebInclusiva.mp4',
                        images: [
                            {fileName: 'webinclusiva-imagen1.png'},
                            {fileName: 'webinclusiva-imagen2.png'},
                            {fileName: 'webinclusiva-imagen3.png'}
                        ]
                    },
                ]
            },
            {
                id: 7,
                title: 'Estación 7',
                subtitle: 'Enseñar, Analizar, Recrear',
                description: null,
                descriptionAudio: 'audioEstacion7.mp3',
                projects: [
                    {
                        id: 1,
                        title: ' Escuelas TIC',
                        description: `<p>
Es un proyecto sobre el aprendizaje significativo de la informática en la escuela, atendiendo al proceso de formación de los ciudadanos del siglo XXI que requiere de nuevas habilidades y destrezas vinculadas a diseñar,  crear e inventar con medios digitales advirtiendo los riesgos de seguridad y privacidad implicados en estas nuevas tecnologías.   En este proyecto se pone especial atención en los siguientes temas y su aplicación en la escuela:  
<ul>
<li>
Enseñanza de programación en la escuela primaria y secundaria. 
</li>
<li>
Robótica educativa. 
</li>
<li>
Gammification (Ludificación).
</li>
<li>
Uso seguro y responsable de las TIC.
</li>
<li>
Creación de APPs. 
</li>
<li>
Aplicaciones educativas libres para dispositivos móviles. 
</li>
<li>
Videojuegos en el proceso de enseñanza-aprendizaje. 
</li>
<li>
RA (Realidad Aumentada) y RV (Realidad Virtual) en usos educativos. 
</li>
</ul>
</p>`,
                        descriptionAudio: 'audioEscuela.mp3',
                        authors: `<p>Laboratorio de Investigación en Nuevas Tecnologías Informáticas (LINTI)</p>`,
                        mainImage: null,
                        mainVideo: 'videoEscuela.mp4',
                        images: [
                            {fileName: 'escuela-imagen1.png'},
                            {fileName: 'escuela-imagen2.png'},
                            {fileName: 'escuela-imagen3.png'},
                            {fileName: 'escuela-imagen4.png'},
                            {fileName: 'escuela-imagen5.png'},
                            {fileName: 'escuela-imagen6.png'},
                            {fileName: 'escuela-imagen7.png'},
                            {fileName: 'escuela-imagen8.png'},
                            {fileName: 'escuela-imagen9.png'},
                            {fileName: 'escuela-imagen10.png'},
                            {fileName: 'escuela-imagen11.png'},
                            {fileName: 'escuela-imagen12.png'},
                            {fileName: 'escuela-imagen13.png'},
                            {fileName: 'escuela-imagen14.png'},
                        ]
                    },
                    {
                        id: 2,
                        title: 'Formación en ciberseguridad: competencias CTF',
                        description: `
<strong>Ciberseguridad: la importancia de la formación en competencias de tipo CTF.</strong>
<p>Los integrantes del equipo de investigación en ciberseguridad del LINTI junto a un grupo de estudiantes de informática participan regularmente de competencias CTF (Capture The Flag) nacionales e internacionales, en las cuales ha logrado consolidarse como equipo obteniendo importantes logros.</p>
<p>Las competencias de tipo CTF consisten en capturar la bandera de los equipos enemigos y resolver problemas de seguridad. Estas competencias están pensadas como un ejercicio de entrenamiento que permite a los participantes adquirir experiencia en el seguimiento de una intrusión, así como trabajar las capacidades de reacción ante ciberataques análogos que suceden en el mundo real.</p> 
<p>Algunas de las competencias de las que venimos participando desde el año 2005 CIPHER, UCSB iCTF, Ekoparty, Da.Op3n, RuCTF, Incibe Cybercamp y OEA Cyberex, entre otras.</p>
`,
                        descriptionAudio: 'audioCTF.mp3',
                        authors: null,
                        mainImage: 'ctf-imagen1.png',
                        mainVideo: null,
                        images: [
                            {fileName: 'ctf-imagen1.png'},
                            {fileName: 'ctf-imagen2.png'},
                            {fileName: 'ctf-imagen3.png'},
                            {fileName: 'ctf-imagen4.png'},
                            {fileName: 'ctf-imagen5.png'}
                        ]
                    },
                ]
            },
            {
                id: 8,
                title: 'Estación 8',
                subtitle: 'Observar, Explorar, Crear',
                description: null,
                descriptionAudio: 'audioEstacion8.mp3',
                projects: [
                    {
                        id: 1,
                        title: 'IoT- Tecnología en Deporte',
                        description: `
<h4>Internet de las cosas (IoT)
 ¿De qué se trata?</h4>
<p>Existe un universo de dispositivos, distribuidos en el mundo, con uno o más sensores que transmiten mediciones a través de Internet a algún sistema, típicamente en la nube, donde esos datos se almacenan para ser procesados. Estos dispositivos también pueden recibir órdenes para cambiar su funcionamiento. </p>
 <p>IoT incorpora objetos que, en un principio, se encontraban fuera del espectro de la web y  abarca desde electrodomésticos hasta robots. El objetivo es lograr que los dispositivos generen datos que permitan monitorear, conectar y controlar a distancia objetos tradicionales y nuevos nodos. Estas posibilidades abren un abanico de escenarios donde puede aplicarse IoT.</p>
<br>
<p>La planificación de las ciudades del futuro deberá incorporar muchas de estas tecnologías que hoy se mencionan como parte del paradigma IOT, a fin de mejorar la calidad de vida de sus ciudadanos y su sostenibilidad ambiental.</p>

<p>Nuevas redes de comunicación están siendo desplegadas en las ciudades para dar soporte a la conexión de estos millones de dispositivos, que hasta hoy no necesitaban conectividad. El surgimiento de nuevas tecnologías para comunicación inalámbrica será fundamental para el desarrollo de las ciudades inteligentes del futuro.  Desde el año 2018 se viene llevando a cabo el despliegue de una red IOT  con tecnología LoRaWAN en la ciudad de La Plata, Berisso y Ensenada,  realizada por especialistas de la Universidad en conjunto con empresas del sector de comunicaciones y constituye un precedente en este campo y abre nuevos escenarios de exploración para la ciencia y los ciudadanos.</p>

<strong>Nuestras soluciones/experiencias IoT</strong>  
<strong>Smart cities:</strong> luminarias Inteligentes, monitoreo de calidad de aire, semáforos Inteligentes, recolección inteligente de residuos, balizamiento de puertos.
 <strong>Agro:</strong> tracking de ganado, análisis inteligente de imágenes de ecografía, análisis inteligente de imágenes para sanidad animal, pastoreo virtual.  
<ul>
<li>
Smart cities
</li>
<li>
Luminarias Inteligentes
</li>
<li>
Monitoreo de calidad de aire
</li>
<li>
Balizamiento Puertos
</li>
<li>
Bombas de Agua
</li>
<li>
Semáforos Inteligentes
</li>
<li>
Recolección Inteligente de Residuos
</li>
<li>
Tracking de Ganado
</li>
</ul>

<p>
La tecnología en el deporte viene sumando adeptos y los entrenamientos modernos apelan cada vez más a ella. Es indudable que las nuevas tecnologías sirven para mejorar el entrenamiento, controlarlo y reducir el margen de error., El uso de estos dispositivos, que antes estaba limitado a los entrenamientos, hoy pueden ser usados  durante las competencias deportivas profesionales. 
Aprovechando los nuevos dispositivos electrónicos para tracking y análisis de performance, se propone desarrollar software que les permita a los preparadores físicos analizar información suministrada en tiempo real por tales dispositivos.   En particular se trabajará con dispositivos de posicionamiento global o GPS y acelerómetros para registrar información referente a la velocidad y distancia recorrida durante situaciones de juego. Esta información, sumada a los datos provenientes de otras fuentes como videos, tagueos, etc., constituyen una fuente de datos grande y muy rica a partir de la cual se puede aplicar  analítica avanzada para brindar a los entrenadores datos procesados, cruzados y gráficos significativos en tiempo real.
Objetivos del proyecto 
<ul>
<li>
	Obtener ventajas competitivas
</li>
<li>
	Mejorar compra y venta de jugadores
</li>
<li>
	Reducir lesiones de jugadores
</li>
<li>
	Mejorar aspectos físico/psicológicos de los jugadores
</li>
<li>
	Profundizar el análisis del juego
</li>
</ul>
</p>`,
                        descriptionAudio: 'audioIoT.mp3',
                        authors: `<p>Laboratorio de Investigación en Nuevas Tecnologías Informáticas (LINTI)</p>`,
                        mainImage: null,
                        mainVideo: null,
                        images: null
                    },
                ]
            },
            {
                id: 9,
                title: 'Estación 9',
                subtitle: 'Indagar, Aplicar, Hacer',
                description: null,
                descriptionAudio: 'audioEstacion9.mp3',
                projects: [
                    {
                        id: 1,
                        title: 'E-Basura',
                        description: `<p>
El <strong>Programa E-Basura</strong> es una iniciativa de la Universidad Nacional de la Plata integrado por un grupo de docentes, alumnos, becarios, graduados y no docentes, preocupados por la problemática ambiental y social generada por el avance tecnológico. 
El <strong>Programa E-Basura</strong> busca concientizar a la sociedad sobre la problemática de los residuos electrónicos, tener un <strong>"rol activo"</strong> y de <strong>"innovación social y ambiental"</strong> promoviendo distintas acciones que se transformen en beneficios y oportunidades para la sociedad. Sus pilares son la inclusión digital, la equidad social, la protección ambiental y la educación en todos los niveles. Para ello se realizan diversas acciones.
<u>Reciclaje y Reacondicionamiento:</u> El programa recibe equipamiento informático en desuso, el cual restaura y reacondiciona, poniéndolo en condiciones operativas, instalándole sistema operativo y aplicaciones educativas, 
<u>Donaciones:</u> Los equipos recuperados luego son donados a instituciones sin fines de lucro y de bien público, a sectores desfavorecidos de la sociedad, plan de becas para alumnos de la UNLP, contribuyendo así en el acercamiento de la tecnología a diversos sectores acortando la brecha social-digital.
<u>Capacitaciones y Cursos:</u> Esta acción se lleva adelante con el dictado de diversos cursos de oficio en armado y reparación de PC, las actividades se realizan en conjunto con la Escuela Universitaria de Oficios (EUO) y con Bienestar Universitario de la UNLP. También se desarrollan prácticas pre-profesionales con alumnos del ultimo año de escuelas técnicas, prácticas profesionales para alumnos de la UNLP y pasantías académicas. Además, es un espacio de formación laboral de alumnos de la UNLP.
<u>Educación ambiental:</u> Organización de diversos eventos y de campañas de recolección y de sensibilización destinados a escuelas de todos los niveles, y para toda la sociedad.
El programa cuenta con 10 años de experiencia, varios convenios y acuerdos firmados tanto a nivel nacional como internacional. Ha recibido varios premios y distinciones desde sus inicios hasta ser formalmente incluido en el Plan Estratégico de la UNLP.
</p>`,
                        descriptionAudio: 'audioEbasura.mp3',
                        authors: null,
                        mainImage: null,
                        mainVideo: 'videoEbasura.mp4',
                        images: [
                            {fileName: 'ebasura-imagen1.png'},
                            {fileName: 'ebasura-imagen2.png'},
                            {fileName: 'ebasura-imagen3.png'},
                            {fileName: 'ebasura-imagen4.png'},
                            {fileName: 'ebasura-imagen5.png'},
                            {fileName: 'ebasura-imagen6.png'},
                            {fileName: 'ebasura-imagen7.png'},
                        ]
                    }
                ]
            }
           /* {
                id: 10,
                title: 'Estación 10 - Polo IT',
                subtitle: 'Creá, Generá, Contribuí',
                description: null,
                descriptionAudio: null,
                projects: [
                    {
                        id: 1,
                        title: 'Preventgas',
                        description: `<p><strong>Preventgas</strong> es una APP móvil,que le permite a los usuarios monitorear
detectores de gases que en altos niveles puede ser perjudiciales para la
salud. El monóxido de carbono y metano, son gases que se producen por la
mala combustión, inoloros, y los detectores de gas son la solución para
estar alertas. La app nace para hacer ésta información visible para el
usuario.</p>

<p>Desde esta app, el usuario, podrá realizar un seguimiento completo
permitiendo estar alerta acerca de toda la información que brinda el
detector. Ante cualquier cambio que se detecte, el usuario recibe
notificaciones visualizando al instante los niveles de gases en un ambiente.</p>

<p>El detector, se vincula con la app mediante el uso de WIFI. Con la
configuración inicial, se define el espacio en donde estará ubicado el
dispositivo, permitiendo agregar más de un dispositivo. </p>

<p>De ésta forma, se une un dispositivo analógico, como es el detector de
gases, con un producto digital, ofreciendo un servicio completo que brinda
a los usuarios información de vital importancia.</p>

<p>Permite a la empresa acercarse a los usuarios, estando presente en todo
momento.</p>`,
                        descriptionAudio: null,
                        authors: `<p> Polo IT </p>`,
                        mainImage: "preventgas-imagen4.png",
                        mainVideo: null,
                        images: [
                            {fileName: 'preventgas-imagen1.png'},
                            {fileName: 'preventgas-imagen2.png'},
                            {fileName: 'preventgas-imagen3.png'},
                            {fileName: 'preventgas-imagen4.png'},
                            {fileName: 'preventgas-imagen5.png'},
                        ]
                    }
                ]
            }*/
        ]
    }];
    currentStation: any;
    constructor() {
    }

    getBuildingData() {
        return this.buildingData;
    }

    getStationData(id: number) {
        let estacion: any;
        this.buildingData.filter((floor: any) => {
            estacion = floor.stations.filter((station: any) => {
                return station.id === id;
            });
        });
        this.currentStation = estacion[0];
        localStorage.setItem('currentStation', JSON.stringify(this.currentStation));
        return this.currentStation;
    }

    getProjectData(id: number) {
        let currentProject: any;
        if (!this.currentStation) {
            this.currentStation = JSON.parse(localStorage.getItem('currentStation'));
        }
        currentProject = this.currentStation.projects.filter((project: any) => {
            return project.id === id;
        });
        return currentProject[0];
    }
}
