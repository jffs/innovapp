import { Component } from '@angular/core';
import {ModalController} from '@ionic/angular';
import {ModalVideoComponent} from './components/modal-video/modal-video.component';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
  constructor( private modalController: ModalController) {
  }
  async openModalVideo() {
    const modal = await this.modalController.create({
      component: ModalVideoComponent,
      cssClass: 'presentationModal',
      animated: true,
    });
    return await modal.present();
  }

}
