import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-modal-video',
  templateUrl: './modal-video.component.html',
  styleUrls: ['./modal-video.component.scss'],
})
export class ModalVideoComponent implements OnInit {
  showSlides = false;
  constructor() {
    window.onload = () => {
      console.log('READY TO PLAY');

    };
  }

  ngOnInit() {
    setTimeout(() => {
      this.showSlides = true;
    }, 0);
  }
  ionViewWillEnter() {
    console.log('READY TO PLAY');

  }
  presentationVideo() {
    return './assets/videos/presentacion.mp4';
  }
}
