import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import {DataService} from "../../services/data.service";
import {AudioService} from "../../services/audio.service";
import {Events, NavController} from '@ionic/angular';

@Component({
  selector: 'app-estacion-detail',
  templateUrl: './estacion.page.html',
  styleUrls: ['./estacion.page.scss'],
})
export class EstacionPage implements OnInit {
    currentStation: any;
    isPlaying: boolean;
  constructor(private activatedRoute: ActivatedRoute,
              public dataService: DataService,
              public audioService: AudioService,
              private events: Events,
              public navCtrl: NavController) {
  }

  ngOnInit() {
      this.activatedRoute.params.subscribe(params => {
          console.log(params);
          this.currentStation = this.dataService.getStationData(parseInt(params.id, 10));
          console.log(this.currentStation);
      });
  }
  handleAudio() {
      if (this.isPlaying) {
          this.audioService.stopAudio();
          this.isPlaying = false;
      } else {
          this.audioService.playAudio(this.currentStation.descriptionAudio);
          this.events.subscribe('audio:finish', () => {
              this.audioService.stopAudio();
              this.isPlaying = false;
          });
          this.isPlaying = true;
      }
  }

    ionViewWillLeave() {
        if (this.isPlaying) {
            this.audioService.stopAudio();
            this.isPlaying = false;
        }
  }
    back() {
        this.navCtrl.back();
    }
}
