import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProyectoPage } from './proyecto.page';

describe('ProyectoPage', () => {
  let component: ProyectoPage;
  let fixture: ComponentFixture<ProyectoPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProyectoPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProyectoPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
