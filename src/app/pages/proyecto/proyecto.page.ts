import { Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {DataService} from '../../services/data.service';
import {AudioService} from '../../services/audio.service';
import {Events, ModalController, NavController} from '@ionic/angular';
import {ModalImageComponent} from './components/modal-image/modal-image.component';

@Component({
    selector: 'app-proyecto',
    templateUrl: './proyecto.page.html',
    styleUrls: ['./proyecto.page.scss'],
})
export class ProyectoPage implements OnInit {
    currentProject: any;
    selectedOption = 'descrip';
    isPlaying: boolean;
    slideOptsH = {
        initialSlide: 0,
        slidesPerView: this.checkScreen(),
        watchSlidesProgress: true,
        watchSlidesVisibility: true,
        autoHeight: true,
        pagination: {
            el: '.swiper-pagination',
            type: 'progressbar',
        }
    };
    showVideo = false;
    loader: boolean[] = [];
    constructor(private activatedRoute: ActivatedRoute,
                public dataService: DataService,
                public audioService: AudioService,
                public events: Events,
                public modalController: ModalController,
                public navCtrl: NavController
    ) {

    }

    ngOnInit() {
        this.activatedRoute.params.subscribe(params => {
            console.log(params);
            this.currentProject = this.dataService.getProjectData(parseInt(params.id, 10));
            console.log(this.currentProject);
        });
        setTimeout(() => {
            this.showVideo = true;
        }, 0);
        if (this.currentProject.mainVideo) {
            window.onload = () => {
                const element: any = document.getElementById('video');
                element.muted = true;
            };
        }
    }

    getCurrentStationName() {
        return this.dataService.currentStation.title;
    }

    getCurrentStationID() {
        return this.dataService.currentStation.id;
    }

    segmentButtonClicked(option: string) {
        this.selectedOption = option;
    }

    handleAudio() {
        if (this.isPlaying) {
            this.audioService.stopAudio();
            this.isPlaying = false;
        } else {
            this.audioService.playAudio(this.currentProject.descriptionAudio);
            this.events.subscribe('audio:finish', () => {
                this.audioService.stopAudio();
                this.isPlaying = false;
            });
            this.isPlaying = true;
        }
    }

    ionViewWillLeave() {
        if (this.isPlaying) {
            this.audioService.stopAudio();
            this.isPlaying = false;
        }
    }

    imageSrc(fileName: string) {
        return `./assets/imgs/${fileName}`;
    }

    videoSrc(fileName: string) {
        return `./assets/videos/${fileName}`;
    }

    async openModalImage(index: number) {
        console.log('index', index);
        /* const modal = await this.modalController.create({
             component: ModalImageComponent,
             cssClass: 'bgBackground',
             animated: true,
             componentProps: {
                 images: this.currentProject.images,
                 index
             }
             });
         await modal.present();*/
        const modal = await this.modalController.create({
            component: ModalImageComponent,
            cssClass: 'imageModal',
            animated: true,
            componentProps: {
                images: this.currentProject.images,
                index
            }
        });
        return await modal.present();
    }

    dismissSkeleton(imagen: string) {
        console.log(`ya se cargo la ${imagen}`);
    }

    back() {
        this.navCtrl.back();
    }

    checkScreen() {
        const innerWidth = window.innerWidth;
        switch (true) {
            case 401 <= innerWidth && innerWidth <= 700:
                return 2.5;
            case 701 <= innerWidth && innerWidth <= 900:
                return 4.5;
            case 901 <= innerWidth:
                return 4.5;
        }
    }
    imageLoaded(index: number) {
        console.log('listo ', index);
        this.loader[index] = true;
    }
}
