import {Component, Input, OnInit} from '@angular/core';
import { ModalController} from '@ionic/angular';
import {DomSanitizer} from '@angular/platform-browser';

@Component({
  selector: 'app-modal-image',
  templateUrl: './modal-image.component.html',
  styleUrls: ['./modal-image.component.scss'],
})
export class ModalImageComponent implements OnInit {
    slideOpts: any;
    @Input() images: any[];
    @Input() index: any;
    showSlides = false;
    slideOptsH = {
        initialSlide: 0,
        slidesPerView: 1,
    };
    constructor(    public modalCtrl: ModalController,
                    private _sanitizer: DomSanitizer
  ) { }

  ngOnInit() {
      this.slideOptsH.initialSlide = this.index;
      setTimeout(() => {
          this.showSlides = true;
      }, 0);
  }
    getImageURL(fileName: string) {
        return  this._sanitizer.bypassSecurityTrustUrl(`./assets/imgs/${fileName}`);
    }

    ionViewDidEnter() {
        this.slideOpts = {
            initialSlide: this.index ? this.index : 2,
            slidesPerView: 1,
            watchSlidesProgress: true,
            watchSlidesVisibility: true,
            pagination: {
                el: '.swiper-pagination',
                type: 'progressbar',
            },
            zoom: {
                toggle: true,
                containerClass: 'swiper-zoom-container',
                zoomedSlideClass: 'swiper-slide-zoomed'
            }
        };
    }
    closeModal() {
        this.modalCtrl.dismiss();
    }

}
