import { Component, OnInit } from '@angular/core';
import {DataService} from '../../services/data.service';
import {NavController} from '@ionic/angular';

@Component({
  selector: 'app-estaciones',
  templateUrl: './estaciones.page.html',
  styleUrls: ['./estaciones.page.scss'],
})
export class EstacionesPage implements OnInit {
    slideOptsH = {
        initialSlide: 0,
        slidesPerView: 2,
        watchSlidesProgress: true,
        watchSlidesVisibility: true,
        pagination: {
            el: '.swiper-pagination',
            type: 'progressbar',
        }
    };
    buildingData: any;
  constructor(public dataService: DataService,
              public navCtrl: NavController) { }

  ngOnInit() {
      this.buildingData = this.dataService.getBuildingData();
  }
    back() {
        this.navCtrl.back();
    }
}
