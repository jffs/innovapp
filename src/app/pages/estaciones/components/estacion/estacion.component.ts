import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-estacion',
  templateUrl: './estacion.component.html',
  styleUrls: ['./estacion.component.scss'],
})
export class EstacionComponent implements OnInit {
  @Input() station: any;
  constructor() {
  }

  ngOnInit() {
  }

}
