import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { FormsModule } from '@angular/forms';
import {EstacionComponent} from './estacion/estacion.component';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
    ],
    exports: [EstacionComponent],
    declarations: [EstacionComponent]
})
export class EstacionComponentsModule {}
