import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { EstacionesPage } from './estaciones.page';
import {EstacionComponentsModule} from "./components/estacion-components.module";
import {HeaderComponent} from '../../components/header/header.component';
import {ComponentsModule} from '../../components/components.module';

const routes: Routes = [
  {
    path: '',
    component: EstacionesPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
      ComponentsModule,
    RouterModule.forChild(routes),
      EstacionComponentsModule
  ],
  declarations: [EstacionesPage]
})
export class EstacionesPageModule {}
